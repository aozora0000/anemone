<?php
$require = require(__DIR__."/../apps/vendor/autoload.php");
use Illuminate\Support\ClassLoader;
/**
 * .envファイル読み出し(環境設定用ファイル)
 */
Dotenv::load(__DIR__);

/**
 * オートローダーの設定
 */
ClassLoader::addDirectories(array(
    __DIR__."/../apps/src/Core",
    __DIR__."/../apps/src/Contracts",
    __DIR__."/../apps/src/Provider",
    __DIR__."/../apps/src/Controller",
    __DIR__."/../apps/src/Model",
    __DIR__."/../apps/src/Mapper/Illuminate",
    __DIR__."/../apps/src/Handler/Session"
));
ClassLoader::register();

/**
 * コンテナ設定
 */
$container = new Pimple\Container;

$container['DEBUG'] = true;

$container['PATH'] = $_SERVER['REQUEST_URI'];

// アプリケーションローダー呼び出し
$container['app'] = function() use ($container) {
    return new App($container);
};

// 設定ファイル読み出し
$container['config'] = function() use ($container) {
    return new Config(__DIR__."/../apps/config/");
};

$container['dispatcher'] = function() use ($container) {
    $dispatcher = new AuraDispatchServiceProvider($container);
    return $dispatcher->getInstance();
};

// ORM設定
$container['ORM'] = function() use ($container) {
    return new EloquentServiceProvider($container['config']->get("database.yaml"));
};

// セッションパラメータ読み出し
$container['session'] = function() use ($container) {
    return new AnemoneSessionServiceProvider(
        new MemcachedSessionHandler(
            MemcachedSessionHandler::config($container['config']->get("memcached.yaml"))
        )
    );
};

// リクエストパラメータ読み出し($container['web']に依存)
$container['request'] = function() use ($container) {
    $factory = new AuraWebFactoryServiceProvider($container);
    return $factory->getRequest();
};
// レスポンスパラメータ読み出し($container['web']に依存)
$container['response'] = function() use ($container) {
    $factory = new AuraWebFactoryServiceProvider($container);
    return $factory->getResponse();
};

// ビューインスタンス呼び出し
$container['view'] = function() use ($container) {
    return new TwigServiceProvider(__DIR__."/../apps/src/View");
};

$container['logger'] = function() use ($container) {
    if($container['DEBUG']) {
        $log = new Monolog\Logger('Logger');
        $log->pushHandler(new Monolog\Handler\BrowserConsoleHandler());
        return $log;
    } else {
        return new stdClass;
    }
};

// メーラー設定
$container['mailer'] = function() use ($container) {
    $mailer = new TwiftMailServiceProvider($container['config']->get("mailer.yaml"));
    return $mailer;
};


/**
 * ディスパッチャー開始
 */
$container['ORM'];
$container['dispatcher'];
