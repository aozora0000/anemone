<?php
/**
 * フレームワーク用コントローラークラス
 */
use Aura\Dispatcher\InvokeMethodTrait;
use Pimple\Container;

class Controller
{
    /**
     * @depencies Aura\Dispacher\InvokeMethodTrait
     */
    use InvokeMethodTrait;

    /**
     * コントローラーコンストラクター
     * @final
     * @param Container $container PimpleDIコンテナを呼び出し
     * @return void
     */
    final public function __construct(Container $container)
    {
        $this->view = new stdClass;
        $this->container = $container;
        $this->request = $container['request'];
        $this->response = $container['response'];
        $this->preProcess();
    }

    /**
     * アクション呼び出し用メソッド
     * @final
     * @param  Array  $params ルーターから渡されたパラメータ
     * @return callable 継承したコントローラーのメソッドを呼びだし
     */
    final public function __invoke(Array $params)
    {
        $action = isset($params['action']) ? $params['action'] : 'index';
        $method = ucfirst($action);
        return $this->invokeMethod($this, $method, $params);
    }

    /**
     * 前処理用メソッド
     * @see __construct
     */
    public function preProcess() {}

    /**
     * 後処理用メソッド
     * @see __destruct
     */
    public function postProcess() {}


    /**
     * テンプレートファイル名受け渡し用メソッド
     * @param String $filename テンプレートファイル名
     */
    final public function render($filename)
    {
        $this->_template = $filename;
    }

    /**
     * コントローラーデストラクタ
     * ビュークラスのrenderメソッドにテンプレート名と変数を渡す
     * @final
     * @return void
     */
    final public function __destruct()
    {
        $this->postProcess();
        $view = $this->container['view'];
        print $view->render($this->_template,(array)$this->view);
    }
}
