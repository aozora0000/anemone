<?php
use Symfony\Component\Finder\Finder;

class Config
{
    /**
     * コンフィグクラスコンストラクター
     * @param String $path 設定ファイルディレクトリパス
     * @return void
     */
    public function __construct($path) {
        $this->dir = $path;
    }

    /**
     * 設定ファイルのフルパス取得メソッド
     * @param  String $name 設定ファイル名
     * @return String       設定ファイルのフルパス
     */
    public function get($name) {
        $finder = new Finder();
        $iterator = $finder->files()->name($name)->depth(0)->in($this->dir);
        $file = array_values(iterator_to_array($iterator));
        return $file[0]->getRealpath();
    }
}
