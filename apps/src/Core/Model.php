<?php
use Pimple\Container;

class Model {
    /**
     * モデルクラスコンストラクター
     * @param Container $container PimpleDIコンテナ格納
     */
    final public function __construct(Container $container)
    {
        $this->container = $container;
    }
}
