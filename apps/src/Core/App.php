<?php
use Pimple\Container;

class App
{
    /**
     * アプリケーションクラスコンストラクター
     * @param Container $container PimpleDIコンテナ
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }
    /**
     * アプリケーションインスタンス呼び出しメソッド
     * @param  String $instance アプリケーション名
     * @return Class            Pimpleコンテナ注入済のアプリケーション
     */
    public function get($instance)
    {
        return new $instance($this->container);
    }

    /**
     * アプリケーションクラスデストラクター
     * @return void
     */
    public function __destruct()
    {
        $this->container = null;
    }
}
