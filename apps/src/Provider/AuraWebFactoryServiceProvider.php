<?php
use Aura\Web\WebFactory;

class AuraWebFactoryServiceProvider
{
    public function __construct(Pimple\Container $container)
    {
        $this->container = $container;
        $this->factory = new WebFactory($GLOBALS);
    }

    public function getResponse()
    {
        return $this->factory->newResponse();
    }

    public function getRequest()
    {
        return $this->factory->newRequest();
    }
}
