<?php
use Illuminate\Database\Capsule\Manager as Cupsule;
use Illuminate\Events\Dispatcher;
use Illuminate\Container\Container;
use \Symfony\Component\Yaml\Yaml;

class EloquentServiceProvider
{

    public $pdo;
    public $cupsule;

    public function __construct($filename)
    {
        if(is_file($filename)) {
            $yaml = Yaml::parse(file_get_contents($filename));
            $config = $yaml[getenv("APP_ENV")];
            $this->cupsule = new Cupsule;
            $this->cupsule->addConnection($config);
            $this->cupsule->setAsGlobal();
            $this->cupsule->setEventDispatcher(new Dispatcher(new Container));
            $this->cupsule->bootEloquent();
        } else {
            throw new \Exception("{$filename} is not found!");
        }
    }

}
