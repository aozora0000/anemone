<?php
use \Symfony\Component\Yaml\Yaml;

class AnemoneCookieServiceProvider implements SessionStorageServiceProviderInterface
{
    protected $cookie;

    public function __construct($filename)
    {
        if(is_file($filename)) {
            $yaml = Yaml::parse(file_get_contents($filename));
            $this->config = $yaml[getenv("APP_ENV")];
            $this->cookie = $_COOKIE;
        } else {
            throw new Exception("{$filename} is not found!");
        }
    }

    public function get($name, $default = null)
    {
        if(isset($this->cookie[$name])) {
            return $this->cookie[$name];
        } else {
            return $default;
        }
    }

    public function set($name, $value, $overdrive = true)
    {
        if($override) {
            $this->cookie[$name] = $value;
            return true;
        } else {
            if(!isset($this->cookie[$name])) {
                $this->cookie[$name] = $value;
                return true;
            }
            return false;
        }
    }

    public function commit()
    {
        $_COOKIE = $this->cookie;
        return true;
    }

    public function __destruct()
    {

    }
}
