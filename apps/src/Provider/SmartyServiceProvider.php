<?php
class SmartyServiceProvider implements ViewServiceProviderInterface
{
    /**
     * Smarty用コンパイルディレクトリ
     * @var string
     */
    public static $compile_dir = "/tmp/";

    /**
     * Smarty呼び出し用
     * @param String $view_dir ビューディレクトリ
     * @throws Exception テンプレートフォルダが存在しない場合
     * @return void
     */
    public function __construct($view_dir)
    {
        if(!is_dir($view_dir)) {
            throw new Exception("テンプレートフォルダ:{$view_dir}が存在しません。");
        }
        $this->smarty = new Smarty;
        $this->smarty->template_dir = realpath($view_dir);
        $this->smarty->compile_dir = self::$compile_dir;
    }

    /**
     * Smartyインスタンスの呼び出し
     * @return Twig_environment
     */
    public function getInstance()
    {
        return $this->smarty;
    }

    /**
     * レンダー用メソッド
     * @param  String $tmpfile テンプレートファイル名
     * @param  Array $object   レンダリング変数格納配列
     * @return String          出力用HTML
     */
    public function render($tplfile,array $object)
    {
        foreach($object as $name=>$value) {
            $this->smarty->assign($name,$value);
        }
        return $this->smarty->fetch($tplfile);
    }
}
