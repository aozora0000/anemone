<?php
use \Symfony\Component\Yaml\Yaml;
class SwiftMailServiceProvider
{
    public function __construct($filename)
    {
        if(is_file($filename)) {
            $yaml = Yaml::parse(file_get_contents($filename));
            $this->config = $yaml[getenv("APP_ENV")];

        } else {
            throw new Exception("{$filename} is not found!");
        }
    }

    private function getSmtpInstance()
    {
        $config = $this->config;
        if($config['ssl']) {
            return Swift_SmtpTransport::newInstance($config['host'], $config['port'],"ssl")
                                    ->setUsername($config['username'])
                                    ->setPassword($config['password']);
        } else {
            return Swift_SmtpTransport::newInstance($config['host'], $config['port'])
                                    ->setUsername($config['username'])
                                    ->setPassword($config['password']);
        }
    }

    public function getMailer()
    {
        return Swift_Mailer::newInstance($this->getSmtpInstance());
    }

    public function getMessage()
    {
        return Swift_Message::newInstance();
    }
}
