<?php
class TwigServiceProvider implements ViewServiceProviderInterface
{
    /**
     * Twig呼び出し用
     * @param String $view_dir ビューディレクトリ
     * @throws Exception テンプレートフォルダが存在しない場合
     * @return void
     */
    public function __construct($view_dir)
    {
        if(!is_dir($view_dir)) {
            throw new Exception("テンプレートフォルダ:{$view_dir}が存在しません。");
        }
        $this->loader = new Twig_Loader_Filesystem(realpath($view_dir));
        $this->twig = new Twig_Environment($this->loader);
    }

    /**
     * Twigインスタンスの呼び出し
     * @return Twig_environment
     */
    public function getInstance() {
        return $this->twig;
    }

    /**
     * レンダー用メソッド
     * @param  String $tmpfile テンプレートファイル名
     * @param  Array $object   レンダリング変数格納配列
     * @return String          出力用HTML
     */
    public function render($tmpfile,array $object) {
        return $this->twig->render($tmpfile, $object);
    }
}
