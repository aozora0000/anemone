<?php
class AnemoneSessionServiceProvider implements SessionStorageServiceProviderInterface
{
    public function __construct($handler = null) {
        if($handler) {
            switch(get_class($handler)) {
                case "PDOSessionHandler":
                case "MongodbSessionHandler":
                case "RedisSessionHandler":
                case "MemcachedSessionHandler":
                case "FileSessionHandler":
                    $handler::init();
                    break;
                default:
                    break;
            }

            ini_set('session.use_cookies',      1               );
            ini_set('session.use_only_cookies', 1               );
            ini_set('session.hash_function',    1               );
            ini_set('session.entropy_file',     '/dev/urandom'  );
            ini_set('session.entropy_length',   32              );
            ini_set('session.use_trans_sid',    0               );
            ini_set('url_rewriter.tags',        'form='         );

            if($_SERVER['SERVER_PORT'] === 443){
                ini_set( 'session.cookie_secure', 1 );
                session_name('SECURE_'.session_name());
            }

            register_shutdown_function('session_write_close');
            session_start();
            session_regenerate_id(true);
            $this->session = $_SESSION;
        }
    }

    public function get($name, $default = null) {
        if(isset($this->session[$name])) {
            return $this->session[$name];
        } else {
            return $default;
        }
    }

    public function set($name, $value ,$override = true) {
        if($override) {
            $this->session[$name] = $value;
            return true;
        } else {
            if(!isset($this->session[$name])) {
                $this->session[$name] = $value;
                return true;
            }
            return false;
        }
    }

    public function commit() {
        $_SESSION = $this->session;
        return true;
    }

    public function __destruct()
    {

    }
}
