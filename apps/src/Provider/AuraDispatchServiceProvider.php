<?php
use \Symfony\Component\Yaml\Yaml;
use Aura\Router\RouterFactory;
use Aura\Dispatcher\Dispatcher;
use Aura\Dispatcher\Exception;
class AuraDispatchServiceProvider
{
    public function __construct(Pimple\Container $container)
    {
        $this->container = $container;

        $router_factory = new RouterFactory;
        $this->router = $router_factory->newInstance();
    }

    public function createRouter($path)
    {
        $yaml = Yaml::parse(file_get_contents($this->container['config']->get("route.yaml")));
        foreach($yaml as $config) {
            if(isset($config['values'])) {
                if(isset($config['token'])) {
                    $this->router->add($config['name'],$config['route'])->addTokens($config['token'])->setValues($config['values']);
                } else {
                    $this->router->add($config['name'],$config['route'])->setValues($config['values']);
                }
            } else {
                $this->router->add($config['name'],$config['route']);
            }
        }
        $path = parse_url($path, PHP_URL_PATH);
        $route = $this->router->match($path, $_SERVER);
        return ($route) ? $route->params : NULL;
    }

    public function createController()
    {
        $yaml = Yaml::parse(file_get_contents($this->container['config']->get("controller.yaml")));
        $container = $this->container;
        if(empty($yaml)) {
            throw new \Exception("コントローラー設定を読み出せませんでした。");
        }
        $controller = array();
        foreach($yaml as $config) {
            $controller[$config['name']] = function () use ($config ,$container) {
                return new $config['class']($container);
            };
        }
        return $controller;
    }

    public function getInstance()
    {
        try {
            $controller = $this->createController();
            $dispatcher = new Dispatcher($controller,"controller","action");
            return $dispatcher($this->createRouter($this->container['PATH']));
        } catch(Exception\ObjectNotDefined $e) {
            (new NotFoundController($this->container))->notFound();
        } catch(Exception\ObjectNotSpecified $e) {
            (new NotFoundController($this->container))->notFound();
        } catch(Exception\ParamNotSpecified $e) {
            (new NotFoundController($this->container))->notFound();
        }
    }
}
