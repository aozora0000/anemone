<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
    <title>{{ title }}</title>
</head>
<style>
html {
    margin: 0;
    padding: 0;
    line-height: 1.5;
    font-family: "Hiragino Kaku Gothic ProN","メイリオ", sans-serif;
    background: #eee;
    text-align: center;
}
#notfoundArea {
    text-align: center;
    padding-bottom:3em;
}
#notfoundArea p {
    margin: 0;
    padding: 100px 0 0 0;
}
</style>
<body>

    <!-- notfoundArea -->
    <div id="notfoundArea">
        <p>{{ message }}</p>
        <p>
            <a href="/" title="TOPへ戻る"></a>
        </p>
    </div>
    <!-- /notfoundArea -->
</body>
</html>
