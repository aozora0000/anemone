<?php

interface ViewServiceProviderInterface
{
    /**
     * ビューインスタンス取得用メソッド
     */
    public function getInstance();

    /**
     * レンダリング用中継メソッド
     * @param  String   $tmpfile テンプレートファイル名
     * @param  Array $obj        レンダリング変数格納配列
     * @return String            出力用HTML
     */
    public function render($tmpfile, array $obj);
}
