<?php

interface SessionStorageServiceProviderInterface
{
    /**
     * セッションストレージ取得用メソッド
     * @param  String   $name    セッションストレージキー
     * @param  Anything $default キーが存在しない場合のデフォルト返り値
     * @return Anything          セッションストレージの内容
     */
    public function get($name, $default = null);

    /**
     * セッションストレージ格納用メソッド
     * @param String   $name     セッションストレージキー
     * @param Anything $value    格納するデータ
     * @param Boolean  $override キーが存在する場合、上書きするか(デフォルトで上書き)
     */
    public function set($name, $value, $override = true);

    /**
     * セッションストレージ保存用メソッド
     * @return void
     */
    public function commit();
}
