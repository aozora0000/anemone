<?php
class NotFoundController extends Controller {
    public function notFound() {
        $this->_template = 'Response/notfound.tpl';
        $this->response->status->set('404', 'Not Found', '1.1');
        header($this->response->status->get(), true, $this->response->status->getCode());
        $this->view->title = "404 Not Found";
        $this->view->message = "ページが見つかりませんでした。";
    }
}
