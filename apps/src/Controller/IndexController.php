<?php
class IndexController extends Controller {
    public function index($page = null) {
        $session = $this->container['session'];
        $this->container["logger"]->info(json_encode($_SESSION));
        $count = $session->get("count",0);
        $session->set("count",$count+1);
        $session->commit();
        //$this->container["app"]->get("Model");
        $postfix = $page+$count;
        $this->view->name = "aozora{$postfix}";
        $this->render("index.tpl");
    }
}
