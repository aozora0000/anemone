<?php
use \Symfony\Component\Yaml\Yaml;

class RedisSessionHandler
{
    /**
     * Redisセッション設定用ホストパス.
     *
     * @var Array
     */
    public static $path;

    /**
     * Redisセッション設定用コンストラクター.
     *
     * @param array $config [tcp://~]を配列として格納、レプリケーション用に配列にしている。
     */
    public function __construct(array $config)
    {
        static::$path = implode(",", $config['host']);

        return true;
    }

    /**
     * Redisセッションイニシャライズメソッド.
     *
     * @TODO 返り値の確認をしたほうが良い？
     */
    public static function init()
    {
        ini_set('session.save_handler', 'redis');
        ini_set('session.save_path', static::$path);

        return (
            ini_get('session.save_handler') === 'redis' &&
            ini_get('session.save_path')    === static::$path
        );
    }

    /**
     * Redisセッション設定用Yamlファイルパースメソッド.
     *
     * @param String $filename Yamlファイルの絶対パス
     *
     * @return Array [tcp://]から始まるRedisのURL
     */
    public static function config($filename)
    {
        $yaml = Yaml::parse(file_get_contents($filename));

        return $yaml[getenv("APP_ENV")];
    }
}
