<?php
use \Symfony\Component\Yaml\Yaml;

class FileSessionHandler
{
    /**
     * Fileセッション設定用ホストパス.
     *
     * @var String
     */
    public static $path;

    /**
     * Fileセッション設定用コンストラクター.
     *
     * @param String $path セッションファイル格納用ディレクトリ
     *
     * @return boolan true
     */
    public function __construct($path)
    {
        if (!is_dir($path) || !is_writable($path)) {
            throw new Exception("{$path}ディレクトリは存在しない、または書き込みが不可能です。");
        }
        self::$path = $path;

        return true;
    }

    /**
     * Fileセッションイニシャライズメソッド.
     *
     * @TODO 返り値の確認をしたほうが良い？
     *
     * @return Boolean ini_setコマンドが成功、パラメータが書き換わったか？
     */
    public static function init()
    {
        ini_set('session.save_handler', 'files');
        ini_set('session.save_path', static::$path);

        return (
            ini_get('session.save_handler') === 'files' &&
            ini_get('session.save_path')    === static::$path
        );
    }

    /**
     * Memcachedセッション設定用Yamlファイルパースメソッド.
     *
     * @param String $filename Yamlファイルの絶対パス
     *
     * @return String 絶対パス形式のファイルパス
     */
    public static function config($filename)
    {
        $yaml = Yaml::parse(file_get_contents($filename));

        return $yaml[getenv("APP_ENV")]["path"];
    }
}
