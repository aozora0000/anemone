<?php
use \Symfony\Component\Yaml\Yaml;

class PDOSessionHandler
{
    private static $table;
    private static $pdo;

    public function __construct($pdo, $table = "php_sessions")
    {
        // テーブルの存在チェック
        $result = $pdo->query("SELECT * FROM {$table} LIMIT 1");
        if (!$result) {
            throw new \Exception("Session Table Not Found!");
        }

        $columns = array();
        $count = $result->columnCount();
        for ($i = 0; $i < $count; ++$i) {
            $meta = $result->getColumnMeta($i);
            $columns[] = $meta['name'];
        }
        if (array('session_id', 'session_updated', 'session_data') != $columns) {
            throw new \Exception("Session FieldName is Differet!");
        }

        self::$pdo = $pdo;
        self::$table = $table;

        return true;
    }

    public static function init()
    {
        return session_set_save_handler(
            array(__CLASS__, 'open'),
            array(__CLASS__, 'close'),
            array(__CLASS__, 'read'),
            array(__CLASS__, 'write'),
            array(__CLASS__, 'destroy'),
            array(__CLASS__, 'gc')
        );
    }

    public static function config($filename)
    {
        $yaml = Yaml::parse(file_get_contents($filename));
        $config = $yaml[getenv("APP_ENV")];

        return new PDO(
            sprintf("mysql:host=%s;dbname=%s;charset=%s", $config['host'], $config['database'], $config['charset']),
            $config['username'],
            $config['password']
        );
    }

    public static function open($savepath, $name)
    {
        return true;
    }

    public static function read($session_id)
    {
        $table = self::$table;
        if (
            $stmt = self::$pdo->prepare(
                        "SELECT
                            session_data
                        FROM {$table}
                        WHERE session_id=:session_id")
        ) {
            $stmt->bindValue(":session_id",     $session_id);
            $stmt->execute();
            $result = $stmt->fetch();

            return (isset($result['session_data'])) ? $result['session_data'] : '';
        } else {
            return "";
        }
    }

    public static function write($session_id, $session_data)
    {
        $affected_rows = 0;
        date_default_timezone_set("UTC");
        $session_updated = time();
        $table = self::$table;
        if (
            $stmt = self::$pdo->prepare(
                        "INSERT INTO {$table} (session_id, session_updated, session_data) VALUES(:session_id, :session_updated, :session_data)
                        ON DUPLICATE KEY UPDATE
                        session_updated=:session_updated,
                        session_data=:session_data")
        ) {
            $stmt->bindValue(":session_id",     $session_id);
            $stmt->bindValue(":session_updated", $session_updated);
            $stmt->bindValue(":session_data",   $session_data);
            $stmt->execute();
            $affected_rows = self::$pdo->lastInsertId();
        }

        return $affected_rows ? true : false;
    }

    public static function destroy($session_id)
    {
        $table = self::$table;
        if (
            $stmt = self::$pdo->prepare(
                        "DELETE FROM {$table} WHERE session_id = :session_id")
        ) {
            $stmt->bind_param(":session_id", $session_id);
            $stmt->execute();
        }

        return true;
    }

    public static function close()
    {
        return true;
    }

    public static function gc($maxlifetime)
    {
        $table = self::$table;
        if (
            $stmt = self::$pdo->prepare(
                        "DELETE FROM {$table}
                        WHERE session_updated < :session_updated")
        ) {
            date_default_timezone_set("UTC");
            $t = time() - $maxlifetime;
            $stmt->bindValue(":session_updated", $t);
            $stmt->execute();
        }

        return true;
    }
}
