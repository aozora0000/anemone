<?php
use \Symfony\Component\Yaml\Yaml;

class MemcachedSessionHandler
{
    /**
     * Memcachedセッション設定用ホストパス.
     *
     * @var Array
     */
    public static $path;

    /**
     * Memcachedセッション設定用コンストラクター.
     *
     * @param array $config [host:port]を配列として格納、レプリケーション用に配列にしている。
     */
    public function __construct(array $config)
    {
        static::$path = implode(",", $config['host']);
    }

    /**
     * Memcachedセッションイニシャライズメソッド.
     *
     * @return Boolean ini_setした内容とini_getした内容が一致するか？
     */
    public static function init()
    {
        ini_set('session.save_handler', 'memcached');
        ini_set('session.save_path', static::$path);

        return (
            ini_get('session.save_handler') === 'memcached' &&
            ini_get('session.save_path')    === static::$path
        );
    }

    /**
     * Memcachedセッション設定用Yamlファイルパースメソッド.
     *
     * @param String $filename Yamlファイルの絶対パス
     *
     * @return Array [host:port]形式のMemcacheのURL
     */
    public static function config($filename)
    {
        $yaml = Yaml::parse(file_get_contents($filename));

        return $yaml[getenv("APP_ENV")];
    }
}
