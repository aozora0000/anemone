<?php
use \Symfony\Component\Yaml\Yaml;

class MongodbSessionHandler
{
    /**
     * @var object Mongoクラスのインスタンス
     */
    private static $mongo = null;
    /**
     * @var string MongoCollectionの名前
     */
    private static $collection = "sessions";

    /**
     * @var object MongoCollectionクラスのインスタンス
     */
    private static $col = null;

    /**
     * mongdbセッション設定用コンストラクタ
     *
     * @param Mongo  $mongo      configメソッドで作成したMongoクラス
     * @param string $collection コレクション名
     *
     * @return boolean true
     */
    public function __construct(\Mongo $mongo, $collection = "sessions")
    {
        self::$mongo = $mongo;
        self::$collection = $collection;

        return true;
    }

    public static function config($filename)
    {
        $yaml = Yaml::parse(file_get_contents($filename));
        $config = $yaml[getenv("APP_ENV")];

        // MongoDB接続オプションの設定
        $options = array(
            'connect' => true,
        );

        // MongoDBへ接続
        self::$mongo = new Mongo($config['host'], $options);
        if (self::$mongo->connected) {
            self::$col = self::$mongo->selectDB($config['database'])->selectCollection(self::$collection);
        }

        return self::$mongo;
    }

    /**
     * mongoSessionの初期化とカスタムセッションハンドラの登録.
     *
     * @return bool セッションハンドラの登録に成功した場合に TRUE を、
     *              失敗した場合に FALSE を返す
     */
    public static function init()
    {
        // カスタムセッションハンドラとして登録
        return session_set_save_handler(
            array(__CLASS__, 'open'),
            array(__CLASS__, 'close'),
            array(__CLASS__, 'read'),
            array(__CLASS__, 'write'),
            array(__CLASS__, 'destroy'),
            array(__CLASS__, 'gc')
        );
    }

    /**
     * セッションがオープンした際に実行されます.
     *
     * @param string $save_path    保存パス
     * @param string $session_name セッション名
     *
     * @return bool 成功した場合に TRUE を、失敗した場合に FALSE を返す
     */
    public static function open($save_path, $session_name)
    {
        return self::$mongo->connected;
    }

    /**
     * セッションの操作が終了した際に実行されます.
     *
     * @return bool 成功した場合に TRUE を、失敗した場合に FALSE を返す
     */
    public static function close()
    {
        return true;
    }

    /**
     * 保存されたセッションデータを読み込みます.
     *
     * @param string $id セッションID
     *
     * @return string セッションデータを返します
     */
    public static function read($id)
    {
        if (!self::$col) {
            return '';
        }
        $item = self::$col->findOne(
            array('_id' => $id), array('data' => true)
        );

        return (string) $item['data'];
    }

    /**
     * セッションデータを保存します.
     *
     * @param string $id        セッションID
     * @param string $sess_data セッションデータ
     *
     * @return bool 成功した場合に TRUE を、失敗した場合に FALSE を返す
     */
    public static function write($id, $sess_data)
    {
        if (!self::$col) {
            return false;
        }

        return self::$col->save(array(
                '_id' => $id,
                'data' => $sess_data,
                'ts' => new MongoDate(), // タイムスタンプ
            ), array('safe' => true));
    }

    /**
     * セッションが session_destroy()  で破棄された際に実行されます。.
     *
     * @param string $id セッションID
     *
     * @return bool 成功した場合に TRUE を、失敗した場合に FALSE を返す
     */
    public static function destroy($id)
    {
        return self::$col->remove(
            array('_id' => $id), array('justOne' => true)
        );
    }

    /**
     * ガベージコレクタが実行されたときに実行されます。.
     *
     * @param int $maxlifetime 最大有効期間
     *
     * @return bool 成功した場合に TRUE を、失敗した場合に FALSE を返す
     */
    public static function gc($maxlifetime)
    {
        return self::$col->remove(
            array(
                // 現在日時より $maxlifetime 以上古いデータは無効
                'ts' => array('$lt' => new MongoDate(time() - $maxlifetime)),
            )
        );
    }
}
