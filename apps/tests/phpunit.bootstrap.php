<?php
require './vendor/autoload.php';
use Illuminate\Support\ClassLoader;

/**
 * オートローダーの設定
 */
ClassLoader::addDirectories(array(
    __DIR__."/../src/Core",
    __DIR__."/../src/Interfaces",
    __DIR__."/../src/Provider",
    __DIR__."/../src/Controller",
    __DIR__."/../src/Model",
    __DIR__."/../src/Mapper/Illuminate",
    __DIR__."/../src/Handler/Session"
));
ClassLoader::register();
