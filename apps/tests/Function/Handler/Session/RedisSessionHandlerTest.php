<?php
/**
 * @group Function
 * @group Handler
 * @group Session
 * @group small
 */
use \Mockery as m;

class RedisSessionHandlerTest extends PHPUnit_Framework_TestCase
{
    public static $host;
    public function setUp()
    {
        self::$host = [
            "host"=>[
                "tcp://127.0.0.1:6379"
            ]
        ];
    }

    public function testRedisSessionHandlerCall()
    {

        $reflection = new ReflectionClass(new RedisSessionHandler(self::$host));
        $this->assertEquals("tcp://127.0.0.1:6379",$reflection->getStaticPropertyValue("path"));
    }

    /**
     * @covers RedisSessionHandler::init
     */
    public function testRedisSessionHandlerInitializeSuccessCase()
    {
        $mock = m::mock(new RedisSessionHandler(self::$host));
        $handler = $mock->shouldReceive('init')->andReturn(true)->getMock();
        $this->assertTrue($handler::init());
    }

    public function testRedisSessionHandlerInitializeFailedCase()
    {
        try {
            $handler = new RedisSessionHandler(self::$host);
            $handler::init();
            $this->fail("Dont Call Exception");
        } catch(\Exception $e) {
            $this->assertTrue(true);
        }
    }

    public function testFileSessionHandlerConfigureCallSuccessCase()
    {
        putenv("APP_ENV=local");
        $path = RedisSessionHandler::config(__DIR__."/../../../../config/redis.yaml");
        $this->assertEquals(self::$host,$path);
    }

    public function testRedisSessionHandlerConfigureCallFailedCase()
    {
        putenv("APP_ENV=local");
        try {
            $path = FileSessionHandler::config("/unexist/path/to/unreadable.yaml");
            $this->fail("ファイル呼び出しができてしまった？");
        } catch(\Exception $e) {
            $this->assertTrue(true);
        }
    }


}
