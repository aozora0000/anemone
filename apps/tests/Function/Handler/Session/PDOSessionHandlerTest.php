<?php
/**
 * @group Function
 * @group Handler
 * @group Session
 * @group small
 */
use \Mockery as m;

class PDOSessionHandlerTest extends PHPUnit_Framework_TestCase
{
    public static $host;
    public function setUp()
    {
        self::$host = [
            "host"=>[
                "localhost"
            ]
        ];
    }

    public function testPDOSessionHandlerCall()
    {

        $reflection = new ReflectionClass(new PDOSessionHandler(new PDO));
        $this->assertEquals("localhost:11211",$reflection->getStaticPropertyValue("path"));
    }

    /**
     * @covers PDOSessionHandler::init
     */
    public function testPDOSessionHandlerInitializeSuccessCase()
    {
        $mock = m::mock(new PDOSessionHandler(self::$host));
        $handler = $mock->shouldReceive('init')->andReturn(true)->getMock();
        $this->assertTrue($handler::init());
    }

    public function testPDOSessionHandlerInitializeFailedCase()
    {
        try {
            $handler = new PDOSessionHandler(self::$host);
            $handler::init();
            $this->fail("Dont Call Exception");
        } catch(\Exception $e) {
            $this->assertTrue(true);
        }
    }

    public function testPDOSessionHandlerConfigureCallSuccessCase()
    {
        putenv("APP_ENV=local");
        $path = PDOSessionHandler::config(__DIR__."/../../../../config/database.yaml");
        $this->assertEquals(self::$host,$path);
    }

    public function testPDOSessionHandlerConfigureCallFailedCase()
    {
        putenv("APP_ENV=local");
        try {
            $path = PDOSessionHandler::config("/unexist/path/to/unreadable.yaml");
            $this->fail("ファイル呼び出しができてしまった？");
        } catch(\Exception $e) {
            $this->assertTrue(true);
        }
    }


}
