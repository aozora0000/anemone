<?php
/**
 * @group Function
 * @group Handler
 * @group Session
 * @group small
 */
use \Mockery as m;

class MemcachedSessionHandlerTest extends PHPUnit_Framework_TestCase
{
    public static $host;
    public function setUp()
    {
        self::$host = [
            "host"=>[
                "localhost:11211"
            ]
        ];
    }

    public function testMemcachedSessionHandlerCall()
    {

        $reflection = new ReflectionClass(new MemcachedSessionHandler(self::$host));
        $this->assertEquals("localhost:11211",$reflection->getStaticPropertyValue("path"));
    }

    /**
     * @covers MemcachedSessionHandler::init
     */
    public function testMemcachedSessionHandlerInitializeSuccessCase()
    {
        $mock = m::mock(new MemcachedSessionHandler(self::$host));
        $handler = $mock->shouldReceive('init')->andReturn(true)->getMock();
        $this->assertTrue($handler::init());
    }

    public function testMemcachedSessionHandlerInitializeFailedCase()
    {
        try {
            $handler = new MemcachedSessionHandler(self::$host);
            $handler::init();
            $this->fail("Dont Call Exception");
        } catch(\Exception $e) {
            $this->assertTrue(true);
        }
    }

    public function testMemcachedSessionHandlerConfigureCallSuccessCase()
    {
        putenv("APP_ENV=local");
        $path = MemcachedSessionHandler::config(__DIR__."/../../../../config/memcached.yaml");
        $this->assertEquals(self::$host,$path);
    }

    public function testMemcachedSessionHandlerConfigureCallFailedCase()
    {
        putenv("APP_ENV=local");
        try {
            $path = MemcachedSessionHandler::config("/unexist/path/to/unreadable.yaml");
            $this->fail("ファイル呼び出しができてしまった？");
        } catch(\Exception $e) {
            $this->assertTrue(true);
        }
    }


}
