<?php
/**
 * @group Function
 * @group Handler
 * @group Session
 * @group small
 */
class FileSessionHandlerTest extends PHPUnit_Framework_TestCase
{
    public function setUp()
    {

    }

    public function testFileSessionHandlerCallSuccessCase()
    {
        $reflection = new ReflectionClass(new FileSessionHandler("/tmp/"));
        $this->assertEquals("/tmp/", $reflection->getStaticPropertyValue("path") ,"can not registor FileSessionHandler");
    }

    public function testFileSessionHandlerCallFailedCase()
    {
        try {
            new FileSessionHandler("/unreadable/path/to/");
            $this->failed("Dont Call Exception");
        } catch(\Exception $e) {
            $this->assertTrue(true);
        }
    }

    public function testFileSessionHandlerInitializeCall()
    {
        $handler = new FileSessionHandler("/tmp/");
        $this->assertTrue($handler::init());
    }

    public function testFileSessionHandlerConfigureCallSuccessCase()
    {
        putenv("APP_ENV=local");
        $path = FileSessionHandler::config(__DIR__."/../../../../config/file.yaml");
        $this->assertEquals("/tmp/",$path);
    }

    public function testFileSessionHandlerConfigureCallFailedCase()
    {
        putenv("APP_ENV=local");
        try {
            $path = FileSessionHandler::config("/unexist/path/to/unreadable.yaml");
            $this->failed("ファイル呼び出しができてしまった？");
        } catch(\Exception $e) {
            $this->assertTrue(true);
        }
    }

}
